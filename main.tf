provider "aws" {        #The provider block configures the specified provider, in this case aws. A provider is a plugin that Terraform uses to create and manage your resources.
  profile = "default"   #aws credentials profile 
  region  = "us-east-2" #region
}

#create a bucket and use this bucket to host a website
resource "aws_s3_bucket" "bucket" {
  bucket = "gp-technical-task" 
  acl    = "public-read"

  website {
    index_document = "index.html"
  }
}

#block public access on this bucket
resource "aws_s3_bucket_public_access_block" "block_access" {
  bucket = aws_s3_bucket.bucket.id
  block_public_acls   = true
  block_public_policy = true
  ignore_public_acls = true
  restrict_public_buckets = true
}

#add policy to grant public access via CloudFront
#added depends on, so first will be create block access. block_access and add_policy at the same time may cause an error.
resource "aws_s3_bucket_policy" "add_policy" {
  depends_on = [
    aws_s3_bucket_public_access_block.block_access,
  ]
  bucket = aws_s3_bucket.bucket.id

  #jsonencode" function converts Terraform expression's result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Id      = "PolicyForCloudFrontPrivateContent"
    Statement = [
      {
        Sid       = "1"
        Effect    = "Allow"
        Principal = {
          AWS = "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity E2FQA65UJRIPTE"
        }
        Action    = "s3:GetObject"
        Resource = "arn:aws:s3:::gp-technical-task/*"
      },
    ]
  })
}

#create CloudFront distribution
resource "aws_cloudfront_distribution" "s3_distribution" {
  origin {
    domain_name = aws_s3_bucket.bucket.bucket_regional_domain_name
    origin_id   = aws_s3_bucket.bucket.bucket_regional_domain_name

    s3_origin_config {
      origin_access_identity = "origin-access-identity/cloudfront/E2FQA65UJRIPTE"
    }
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "Distribution created by terraform"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = aws_s3_bucket.bucket.bucket_regional_domain_name
    cache_policy_id  = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad"
    compress         = true

    viewer_protocol_policy = "allow-all"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}